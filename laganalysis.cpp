#include <opencv2/opencv.hpp>
#include <iostream>
#include <stdio.h>
#include <chrono>
#include "cxxopts.hpp"
#include <functional>
#include <fstream>

const int SEARCH_FOR_INPUT = 0;
const int SEARCH_FOR_REACTION = 1;
const int STABILISATION = 2;

using namespace std::placeholders;

inline float norm2(const cv::Vec3f& vec) {
    return sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);
}

template<bool INVERTED>
int detect_component_scanline(const cv::Mat& frame, int minX, int maxX) {
    int found = -1;
    const int size = maxX - minX;
    const int start = minX;
    for (int y = 0; y < frame.rows; ++y) {
        cv::Vec3f direction = {0, 0, 0};
        for (int x = 0; x < size; ++x) {
            const auto &pixel = frame.at<cv::Vec3b>(y, x+start);
            direction += cv::Vec3f(pixel) / 255.0f;
        }
        direction /= size;
        const float norm = norm2(direction);
        if (norm > 0.1) {
            direction /= norm;
        } else {
            // this is black, we dont care about black
            continue;
        }
        const float dist_to_green = norm2(direction - cv::Vec3f(0,1,0))/1.4142f;
        const float dist_to_magenta = norm2(direction - cv::Vec3f(0.7071f,0,0.7071f))/1.4142f;
        //printf("TRACE blue=%f green=%f red=%f magenta=%f interrupt=%f y=%d\n", direction[0], direction[1], direction[2], dist_to_magenta, dist_to_green, y);
        const bool is_magenta = dist_to_magenta < 0.15;
        const bool is_green = dist_to_green < 0.3;
        const bool positive = INVERTED ? is_magenta : is_green;
        const bool negative = INVERTED ? is_green : is_magenta;
        if (negative) {
            // Magenta line, break!
            //printf("TRACE blue=%f green=%f red=%f magenta=%f interrupt=%f norm=%f y=%d\n", direction[0], direction[1], direction[2], dist_to_magenta, dist_to_green, norm, y);
            //printf("MAGENTA BREAK\n");
            return -1;
        }
        if (found < 0 && positive) {
            // Green line, we've got what we want, keep rolling to make sure we dont find a magenta line
            //printf("TRACE blue=%f green=%f red=%f magenta=%f interrupt=%f y=%d\n", direction[0], direction[1], direction[2], dist_to_magenta, dist_to_green, y);
            found = y;
        }
    }

    return found;
}

inline bool rgb_select_red(int r, int g, int b) {
    return (r < 10) && (g > 15) && (b > 15);
}

inline bool rgb_select_green(int r, int g, int b) {
    return (r > 15) && (g < 10) && (b > 15);
}

inline bool rgb_select_blue(int r, int g, int b) {
    return (r > 15) && (g > 15) && (b < 10);
}

template<bool (*SELECT)(int, int, int), bool INVERTED>
int detect_rgb_scanline(const cv::Mat& frame, int minX, int maxX) {
    const int size = maxX - minX;
    const int start = minX;
    for (int y = 0; y < frame.rows; ++y) {
        int b = 0;
        int g = 0;
        int r = 0;
        for (int x = start; x < size; ++x) {
            const auto &pixel = frame.at<cv::Vec3b>(y, x);
            b += pixel[0];
            g += pixel[1];
            r += pixel[2];
        }
        b /= size;
        g /= size;
        r /= size;
        if (SELECT(r, g, b) == INVERTED) {
            return y;
        }
    }

    return -1;
}

class Detection {
public:
    virtual ~Detection() = default;
    virtual bool detect(const cv::Mat& current, const cv::Mat& previous, cv::Mat* debug) = 0;
};

class FlowDetection: public Detection {
public:
    FlowDetection(double threshold): threshold(threshold) {}

    bool detect(const cv::Mat& current, const cv::Mat& previous, cv::Mat* debug) override {
        cv::cvtColor(current, working, cv::COLOR_BGR2GRAY);
        cv::cvtColor(previous, prev, cv::COLOR_BGR2GRAY);
        cv::calcOpticalFlowFarneback(working, prev, result, 0.5, 3, 15, 3, 5, 1.1, 0);
        cv::Mat angle, mag, current_converted, prev_converted;
        std::vector<cv::Mat> channels(3);
        cv::split(result, channels);
        cv::cartToPolar(channels[0], channels[1], mag, angle);
        cv::threshold(mag, mag,1,255,cv::THRESH_TOZERO);
        double opticflow = cv::sum(mag)[0];
        if (debug != nullptr) {
            cv::Mat abs;
            /*
            float range[] = { 0, 256 }; //the upper boundary is exclusive
            const float* histRange = { range };
            int histSize = 256;
            cv::Mat b_hist;
            cv::calcHist( &mag, 1, 0, cv::Mat(), b_hist, 1, &histSize, &histRange, true, false);
            for (int i = 0 ; i < 256 ; i++) {
                printf("%d => %f\n", i, b_hist.at<float>(i));
            }
            */
            working.convertTo(current_converted, CV_32FC1, 1.0/255);
            prev.convertTo(prev_converted, CV_32FC1, 1.0/255);
            double min, max;
            cv::minMaxLoc(mag, &min, &max);
            max = max > 20 ? max : 20;
            std::vector<cv::Mat> hsv{3};
            hsv[0] = {(angle * (180 / 3.1415926535 / 2))};
            hsv[1] = {(mag - min) / (max-min)};
            hsv[2] = {current_converted * 0.7 + prev_converted * 0.3};
            // display the frame then the opticflow representation
            cv::Mat hsvAssembled{};
            cv::merge(hsv, hsvAssembled);
            cv::cvtColor(hsvAssembled, *debug, cv::COLOR_HSV2BGR);
            std::cout << "\t Sum of magnitude " << opticflow << std::endl;
        }
        return opticflow > this->threshold;
    }
private:
    double threshold;
    cv::Mat working, prev, result;
};

class PatternDetection: public Detection {
public:
    PatternDetection(double threshold, std::vector<std::string> &patterns): threshold(threshold) {
        this->patterns.reserve(patterns.size()*2);
        for(auto &pattern: patterns) {
            std::cout << "\tLoading pattern " << pattern << std::endl;
            const cv::Mat &src = cv::imread(pattern);
            const cv::Mat &blackWhite = this->patterns.emplace_back(src.rows, src.cols, CV_8UC1);
            cv::cvtColor(src, blackWhite, cv::COLOR_BGR2GRAY);
            cv::flip(blackWhite, this->patterns.emplace_back(src.rows, src.cols, CV_8UC1), 1);
        }
    }
    bool detect(const cv::Mat& current, const cv::Mat& previous, cv::Mat* debug) override {
        cv::cvtColor(current, working, cv::COLOR_BGR2GRAY);
        double min_val; double max_val; cv::Point min_loc; cv::Point max_loc;
        int n = 0;
        for (cv::Mat& pattern : this->patterns) {
            cv::matchTemplate(working, pattern, result, cv::TM_CCOEFF_NORMED);
            cv::minMaxLoc(result, &min_val, &max_val, &min_loc, &max_loc);

            if (debug != nullptr) {
                cv::Mat copy, overlay;
                debug->copyTo(copy);
                debug->copyTo(overlay);
                cv::rectangle(overlay, max_loc, cv::Point{max_loc.x+pattern.cols, max_loc.y+pattern.rows}, cv::Scalar_{0, 0, 255}, 2);
                double alpha = max_val * max_val;
                cv::addWeighted(copy, 1-alpha, overlay, alpha, 0, *debug);
                std::cout << "\t pattern #" << (n/2 + 1) << ((n % 2 == 0) ? ": " : " (mirror): ") << max_val << std::endl;
            }
            n += 1;
            if (max_val > this->threshold) {
                return true;
            }
        }

        return false;
    }
private:
    double threshold;
    std::vector<cv::Mat> patterns{0};
    cv::Mat working, result;
};

int main(int argc, char **args) {
    std::vector<std::string> patterns;
    cxxopts::Options option_setup("laganalysis", "Analyse a video file to extract lag information");
    option_setup.add_options()
            ("i,input", "Input video file", cxxopts::value<std::string>()->default_value(""))
            ("s,scanlines", "Actual scanlines count", cxxopts::value<int>()->default_value("0"))
            ("e,offset", "Global offset of the image", cxxopts::value<int>()->default_value("0"))
            ("n,button", "Button detection algorithm [component,red,blue,green]", cxxopts::value<std::string>()->default_value(""))
            ("h,help", "Display help")
            ("a,algorithm", "Frame detection algorithm [flow,pattern]", cxxopts::value<std::string>()->default_value(""))
            ("t,threshold", "Detection threshold", cxxopts::value<double>()->default_value("0"))
            ("d,debug", "Debug mode")
            ("f,frame", "Frame to start from", cxxopts::value<int>()->default_value("0"))
            ("x,minX", "Minimal X view to look at", cxxopts::value<int>()->default_value("0"))
            ("X,maxX", "Maximal X view to look at", cxxopts::value<int>()->default_value("0"))
            ("y,minY", "Minimal Y view to look at", cxxopts::value<int>()->default_value("0"))
            ("Y,maxY", "Maximal Y view to look at", cxxopts::value<int>()->default_value("0"))
            ("bMinX", "Minimal X used for button detection", cxxopts::value<int>()->default_value("0"))
            ("bMaxX", "Maximal X used for button detection", cxxopts::value<int>()->default_value("0"))
            ("p,pattern", "Pattern files to look for using the pattern algorithm", cxxopts::value(patterns))
            ("o,output", "Output file", cxxopts::value<std::string>()->default_value("-"))
            ("v,inverted", "Inverted detection: if the interruption happens when the button is NOT pressed")
            ;

    try {
        cxxopts::ParseResult options = option_setup.parse(argc, args);
        if (options.count("help")) {
            std::cout << option_setup.help({""}) << std::endl;
            exit(0);
        }

        const auto &video_path = options["input"].as<std::string>();
        auto video = cv::VideoCapture(video_path);
        if (!video.isOpened()) {
            std::cerr << "Cannot open video " << video_path << std::endl;
            exit(5);
        }

        const auto height = static_cast<const int>(video.get(cv::CAP_PROP_FRAME_HEIGHT));
        const auto width = static_cast<const int>(video.get(cv::CAP_PROP_FRAME_WIDTH));
        const auto format = static_cast<const int>(video.get(cv::CAP_PROP_FORMAT));
        std::cout << "Loaded video " << video_path << " in format " << width << "x"<< height << "\n";

        cv::Mat buffer1{height, width, format};
        cv::Mat buffer2{height, width, format};


        const auto reversed = options.count("inverted") > 0;
        int bMinX = options["bMinX"].as<int>();
        int bMaxX = options["bMaxX"].as<int>();
        if (bMaxX == 0) {
            bMaxX = width;
        }

        int (*input_detection)(const cv::Mat &, int minX, int maxX) = nullptr;
        auto &button_detection = options["button"].as<std::string>();
        if (button_detection == "component") {
            input_detection = reversed ? detect_component_scanline<true> : detect_component_scanline<false>;
        } else if (button_detection == "red") {
            input_detection = reversed ? detect_rgb_scanline<rgb_select_red, true> : detect_rgb_scanline<rgb_select_red, false>;
        } else if (button_detection == "blue") {
            input_detection = reversed ? detect_rgb_scanline<rgb_select_blue, true> : detect_rgb_scanline<rgb_select_blue, false>;
        } else if (button_detection == "green") {
            input_detection = reversed ? detect_rgb_scanline<rgb_select_green, true> : detect_rgb_scanline<rgb_select_green, false>;
        } else {
            std::cerr << "Invalid button detection algorithm " << button_detection << std::endl;
            exit(3);
        }
        std::cout << "Selecting "<< button_detection << " input button detection" << std::endl;

        auto &algorithm = options["algorithm"].as<std::string>();
        Detection* detection;
        std::unique_ptr<FlowDetection> flow;
        std::unique_ptr<PatternDetection> pattern;
        if (algorithm == "flow") {
            std::cout << "Selecting FlowDetection algorithm" << std::endl;
            flow = std::make_unique<FlowDetection>(options["threshold"].as<double>());
            detection = flow.get();
        } else if (algorithm == "pattern") {
            std::cout << "Selecting PatternDetection algorithm" << std::endl;
            pattern = std::make_unique<PatternDetection>(options["threshold"].as<double>(), patterns);
            detection = pattern.get();
        } else {
            std::cerr << "Invalid frame detection algorithm " << algorithm << std::endl;
            exit(4);
        }
        const auto debug = options.count("debug") > 0;

        std::ostream *out = &std::cout;
        std::string outpath = options["output"].as<std::string>();
        std::unique_ptr<std::ofstream> outstream;
        if (outpath != "-") {
            outstream = std::make_unique<std::ofstream>(outpath);
            out = reinterpret_cast<std::ostream*>(outstream.get());
        }

        int n = 0;
        int lag_mode = 0;
        int frame = 0;
        int scanline = 0;
        int total_scanlines = options["scanlines"].as<int>();
        int offset = options["offset"].as<int>();
        int start_frame = options["frame"].as<int>();
        const auto &maxX = options["maxX"].as<int>();
        const auto &maxY = options["maxY"].as<int>();
        const auto &minX = options["minX"].as<int>();
        const auto &minY = options["minY"].as<int>();
        cv::Rect rect = cv::Rect{minX, minY, (maxX <= 0 ? width : maxX)-minX, (maxY <= 0 ? height : maxY)-minY};
        cv::Mat debug_mat{rect.height, rect.width, format};

        std::cout << "We're ready!" << std::endl;

        *out << "frame,line,lag" << std::endl;
        for (;;) {
            cv::Mat &current_frame = (n % 2 == 0) ? buffer1 : buffer2;
            cv::Mat &previous_frame = (n % 2 == 0) ? buffer2 : buffer1;
            n += 1;
//            std::cout << "read frame " << n << std::endl;
            if (!video.read(current_frame)) {
                break;
            }
            if (n < start_frame) {
                continue;
            }

            if (lag_mode != SEARCH_FOR_REACTION) {
//                std::cout << "Detecting input " << n << std::endl;
                scanline = input_detection(current_frame, bMinX, bMaxX);
//                if (debug) {
//                    cv::imshow("debug", current_frame);
//                    cv::waitKey();
//                }
                if (lag_mode == SEARCH_FOR_INPUT) {
                    if (scanline >= 0) {
                        lag_mode = SEARCH_FOR_REACTION;
                        frame = 0;
                        if (debug) {
                            std::cout << "[" << n << "] Found a button press on scanline " << scanline << std::endl;
                            cv::imshow("debug", current_frame);
                            cv::waitKey();
                        }
                    }
                } else {
                    if (scanline < 0) {
                        lag_mode = SEARCH_FOR_INPUT;
                    }
                }
            } else {
                frame += 1;
//                std::cout << "Detecting frame " << n << std::endl;
                if (debug) {
                    current_frame(rect).copyTo(debug_mat);
                    std::cout << "[" << n << "] Trying to detect the reaction frame" << std::endl;
                }
                bool detect = detection->detect(current_frame(rect), previous_frame(rect), debug ? &debug_mat : nullptr);
                if (frame == 1) {
                    detect = false;
                }
                if (debug) {
                    cv::imshow("debug", debug_mat);
                    cv::waitKey();
                }
                if (detect) {
                    lag_mode = STABILISATION;
                    int actual_scanline = scanline - offset;
                    if (actual_scanline <= 0) {
                        actual_scanline = 0;
                    }
                    if (total_scanlines != 0) {
                        actual_scanline = (actual_scanline * total_scanlines) / height;
                    }
                    *out << n << "," << actual_scanline << "," << frame << std::endl;
                } else if (frame == 15) {
                    lag_mode = STABILISATION;
                    if (debug) {
                        std::cout << "[" << n << "] Couldnt detect the reaction frame... back to normal" << std::endl;
                    }
                }
            }
        }

    } catch (const cxxopts::OptionParseException &e) {
        std::cout << "error parsing options: " << e.what() << std::endl;

        std::cout << option_setup.help({""}) << std::endl;
        exit(1);
    }
    // the videofile will be closed and released automatically in VideoWriter destructor
    return 0;
}