#include <opencv2/opencv.hpp>
#include <iostream>
#include <stdio.h>
#include <chrono>
#include "cxxopts.hpp"

int main(int argc, char **args) {
    cxxopts::Options option_setup("streamcatcher", "Capture a video stream in video file");
    option_setup
            .positional_help("[optional args]")
            .show_positional_help();
    option_setup
            .add_options()
            ("f,fps", "FPS base", cxxopts::value<double>()->default_value("60"))
            ("h,help", "Display help")
            ("v,view", "Dont write, just view")
            ("x,width", "Desired width", cxxopts::value<int>()->default_value("0"))
            ("y,height", "Desired height", cxxopts::value<int>()->default_value("0"))
            ("c,capturedevice", "Capture device number", cxxopts::value<int>()->default_value("0"))
            ("d,dir", "Dir base", cxxopts::value<std::string>()->default_value("."))
            ("n,nodisplay", "Don't display the stream")
            ("p,prefix", "Prefix to use", cxxopts::value<std::string>()->default_value("streamcatcher"))
            ("s,frameskip", "Number of frames to skip in the display", cxxopts::value<int>()->default_value("0"));

    try {
        cxxopts::ParseResult options = option_setup.parse(argc, args);
        if (options.count("help")) {
            std::cout << option_setup.help({""}) << std::endl;
            exit(0);
        }
        double fps = options["fps"].as<double>();
        std::string prefix = options["prefix"].as<std::string>();
        std::string dir = options["dir"].as<std::string>();
        bool nodisplay = options["nodisplay"].as<bool>();
        int frameskip_modulo = options["frameskip"].as<int>() + 1;
        int capturedevice = options["capturedevice"].as<int>();

        if (fps < 10 || fps > 300) {
            std::cerr << "Something's wrong with the fps value: " << args[1] << "\n";
            exit(2);
        }
        std::cout << "Desired fps: " << fps << "\n";

        std::cout << "GOGOGOGO!\n";
        cv::Mat src, display;
        cv::Point frame_origin(0, 15);
        cv::Point fps_origin(0, 30);
        cv::Scalar font_color(255, 255, 255);
        // use default camera as video source
        cv::VideoCapture cap;
        {
            int height = options["height"].as<int>();
            int width = options["width"].as<int>();
            if (height != 0 && width != 0) {
                cap.set(cv::CAP_PROP_FRAME_WIDTH, width);
                cap.set(cv::CAP_PROP_FRAME_HEIGHT, height);
            }
        }
        cap.open(capturedevice);
        // check if we succeeded
        if (!cap.isOpened()) {
            std::cerr << "ERROR! Unable to open camera\n";
            return -1;
        }
        // get one frame from camera to know frame size and type
        cap >> src;
        // check if we succeeded
        if (src.empty()) {
            std::cerr << "ERROR! blank frame grabbed\n";
            return -1;
        }

        char now_str[26];
        {
            std::chrono::system_clock::time_point p = std::chrono::system_clock::now();
            time_t t = std::chrono::system_clock::to_time_t(p);
            struct tm timeinfo;
            localtime_s(&timeinfo, &t);
            strftime(now_str, sizeof(now_str), "%Y%m%d-%H%M%S", &timeinfo);
        }
        bool isColor = (src.type() == CV_8UC3);
        //--- INITIALIZE VIDEOWRITER
        cv::VideoWriter writer;
        if (!options.count("view")) {
            int codec = cv::VideoWriter::fourcc('X', 'V', 'I', 'D');  // select desired codec (must be available at runtime)
            std::string filename = prefix + "-" + now_str + ".avi";             // name of the output video file
            //--- GRAB AND WRITE LOOP
            std::cout << "Writing videofile: " << filename << std::endl
                      << "Press any key to terminate" << std::endl;

            writer.open(dir + "\\" + filename, codec, fps, src.size(), isColor);
            // check if we succeeded
            if (!writer.isOpened()) {
                std::cerr << "Could not open the output video file for write\n";
                return -1;
            }
        }
//    std::chrono::time_point t = std::chrono::high_resolution_clock::now();
        int i = 0;
        auto start = std::chrono::high_resolution_clock::now();

        for (;;) {
            // check if we succeeded
            if (!cap.grab()) {
                std::cerr << "ERROR! blank frame grabbed\n";
                break;
            }
            if (i == 0) {
                start = std::chrono::high_resolution_clock::now();
            }
            double actual_fps = 0.0;
            auto current_time = std::chrono::high_resolution_clock::now();
            cap.retrieve(src);
//        auto temp = std::chrono::high_resolution_clock::now();
//        std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(temp - t).count() << "ns\n";
//        t = temp;
            if (i > 0) {
                actual_fps = i * 1000000000.0 /
                             std::chrono::duration_cast<std::chrono::nanoseconds>(current_time - start).count();
            }
            // encode the frame into the videofile stream

            if (!options.count("view")) {
                writer.write(src);
            }
            i += 1;
            if (!nodisplay && ((i % frameskip_modulo) == 0)) {
                src.copyTo(display);
                char buff[100];
                snprintf(buff, sizeof(buff), "n=%d, %dx%d", i, src.size[0], src.size[1]);
                cv::putText(display, buff, frame_origin, cv::FONT_HERSHEY_PLAIN, 1, font_color);
                snprintf(buff, sizeof(buff), "current fps=%.02f", actual_fps);
                cv::putText(display, buff, fps_origin, cv::FONT_HERSHEY_PLAIN, 1, font_color);
                // show live and wait for a key with timeout long enough to show images
                cv::imshow("Live", display);
                if (cv::waitKey(1) >= 0)
                    break;
            }
        }

    } catch (const cxxopts::OptionParseException &e) {
        std::cout << "error parsing options: " << e.what() << std::endl;

        std::cout << option_setup.help({""}) << std::endl;
        exit(1);
    }
    // the videofile will be closed and released automatically in VideoWriter destructor
    return 0;
}