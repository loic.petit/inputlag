# WydD's Input Lag Tools

## Hardware

The method is simple in essence: interrupt parts of the video signal when you press your button and look at the output.

You can interrupt using either a 74HC4053 or any kind of optocoupler. The wiring should be easy if you're a minimum 
electronic-savy.

If you're analysing a HDMI stream, you'll need:
* A hdmi-to-component converter. Then interrupt the `Pr` and `Pb` signals.
* A component capture card 1080p60 (cheap, usually comes with hdmi as well)

If you're analysing a RGB stream (SCART, VGA). Interrupt one of the color (protip: use two channels on your interruption in parallel
to avoid disturbing the impedance too much). You can use the following hardware:
* A framemeister or an OSSC to convert into hdmi and then use a hdmi capture card
* A component encoder and use a component capture card
* A rgb capture card (usually expensive and rarely reusable)

## Streamcatcher

This is a command line tool to dump your capture device stream into a video file. Keeps the original ratio and let you
customize the frame rate. Run the command with `--help` to see how to use it. 

_Warning_ The frame rate in the video file might not correspond to the one you specified due to a 
[bug](https://github.com/opencv/opencv/issues/9023) in `opencv`. It's been fixed but I'm waiting for a new release.

## Laganalysis

This is a command line tool to analyse a video file and provide a three-column csv file as output: the frame, 
the scanline of the input, and the number of frames until the reaction frame. 
Run the command with `--help` to see how to use it. 

### Input detection algorithm

The input is a condition on the average channels of each row. Here are the four available conditions
* `component`: detects the green line when interrupting both `Pr` and `Pb` signals on a component video.
* `red`: detect the interruption of the red line in a RGB signal
* `blue`: detect the interruption of the blue line in a RGB signal
* `green`: detect the interruption of the green line in a RGB signal

Example of a `PrPb` interruption on a component signal.

![PrPb interruption of a component signal](img/component-interrupt.png)

### Reaction frame detection

To detect the reaction frame you have to use one algorithm. They both use a threshold value (option `-t`) and it is 
generally a good idea to restrict the view port of the algorithm to avoid interference (options `-x -X -y -Y`). Don't
hesitate to use the debug option `-d` to adjust those parameters accordingly.

#### Pattern matching algorithm

*When*: Great on 2D scenes when you search for a sprite.

Feed it patterns as image files (option `-p`) and it will try to find them in each frame. If one pattern matches above 
a certain value the match is good. It is a good idea to create patterns on the actual video file (mostly because 
the sizes are really sensitive).

Example of the debug tool:

| Negative | Positive |
|----------|----------|
|![negative pattern detection](img/pattern-detection-negative.png)|![positive pattern detection](img/pattern-detection.png)|

#### Optic flow algorithm

*When*: Great on 3D models when you search for an action. Good default option when you're not sure.

It tries to measure the optic flow between two frames. If something moves above a certain magnitude, the match is considered
valid.

Example of the debug tool:

| Negative | Positive |
|----------|----------|
|![negative flow detection](img/flow-detection-negative.png)|![positive flow detection](img/flow-detection.png)|

The colour patches are where movement has been seen (on the negative you can see a blur of green around snowflakes). The
colour should indicate the direction of the movement. 

## Build

You need to have OpenCV somewhere. Then bind your CMake with `-DOPENCV_DIR=...`, give it the path of the binary distribution (e.g. `build\x64\vc15` in my case).

If you need binary build, ping me. 